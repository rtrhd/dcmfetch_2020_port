.PHONY: build install tests clean
build:
	python setup.py build

install:
	python setup.py install
	if [ ! -e /etc/dcmnodes.cf ]; then install -m 644 dcmnodes.cf /etc; fi

tests:
	(cd tests; make tests)

clean:
	rm -rf build dist dcmfetch.egg-info dcmfetch/*.pyc dcmfetch/__pycache__
	(cd docs; make clean)

