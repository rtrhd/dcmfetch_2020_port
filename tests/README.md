To test this package properly need a DICOM server to run against.
Although some of this could be *mocked* out, we'll coordinate with a real server here.

For testing the DICOM native protocol we'll use a java based dcm4che3 server `dcmqrscp` that
will serve images given an image directory with a `DICOMDIR` index file.
A bundled executable jar version is included in this directory for POSIX and mswindows.

To test the DICOM rest api we use the standard `docker` image of the Orthanc server and
pre-populate it with images using the bundled dcm4che3 `storescu` program.

So for testing we use the following:

- [pydicom](http://www.pydicom.org/)
  - pip installable or conda installable on the conda-forge channel
- [java](https://www.java.com/)
  - available on POSIX systems, download from oracle for mswin
- [dcm4che3](https://sourceforge.net/projects/dcm4che/files/dcm4che3/)
  - required programs included here for POSIX and mswin
- [dcm4che2](https://sourceforge.net/projects/dcm4che/files/dcm4che2/)
  - install downloaded zip file as `/usr/local/dcm4che2` (or `c:\dcm4che2` on mswin)

- docker (for running [orthanc](http://www.orthanc-server.com/) test server image)
  - on ubuntu: [docker on ubuntu](https://docs.docker.com/engine/installation/linux/ubuntulinux/)
  - on macosx: [docker-for-mac](https://docs.docker.com/docker-for-mac/)
  - on mswin: [docker-for-windows](https://docs.docker.com/docker-for-windows/)

Some of the above may not be available on a mswindows machine so not all tests will be runnable.