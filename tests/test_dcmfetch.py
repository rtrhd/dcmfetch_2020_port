from __future__ import print_function, division

import unittest
import os
import sys
from tempfile import mkdtemp
from glob import glob
from shutil import rmtree
from os.path import join, dirname, abspath
from subprocess import Popen, STDOUT
from time import sleep

try:
    from subprocess import DEVNULL  # py3k
except ImportError:
    DEVNULL = open(os.devnull, 'wb')

TESTDIR = dirname(abspath(__file__))
sys.path.insert(0, abspath(join(TESTDIR, '..')))
TESTDATA = join(TESTDIR, 'testdata')

from dcmfetch.dcmfetch import *

SRVPROG = 'dcmqrscp'
SRVARGS = ['--no-storage', '-b', 'DCMQRSCP:11112', '--dicomdir', join(TESTDATA, 'DICOMDIR')]
os.environ['PATH'] = os.pathsep.join([TESTDIR] + os.environ['PATH'].split(os.pathsep))

NODEFILE = '''
Server DCMQRSCP localhost 11112 FSGX
'''

PATID = 'PQA20160906RHD'
STUDYUID = '1.3.12.2.1107.5.2.19.45064.30000016090616040707700000004'
SERIESUID = '1.3.12.2.1107.5.2.19.45064.2016090617560117683773517.0.0.0'

PATID_B = 'QQA20180418BCHSKYRA'
STUDYUID_B = '1.3.12.2.1107.5.2.19.45622.30000018041807565753300000009'
SERIESUID_B1 = '1.3.12.2.1107.5.2.19.45622.201804181719501256341027.0.0.0'
SERIESUID_B2 = '1.3.12.2.1107.5.2.19.45622.2018041817462338771353016.0.0.0'

NIMAGES = 3


class TestDcmFetch(unittest.TestCase):
    """Tests for functions in dicomweb.py"""
    @classmethod
    def setUpClass(cls):
        cls._process = Popen([SRVPROG] + SRVARGS, stdout=DEVNULL, stderr=STDOUT)
        sleep(1)
        cls._olcwd = os.getcwd()
        cls._tmpwd = mkdtemp()
        os.chdir(cls._tmpwd)
        with open('dcmnodes.cf', 'w') as f:
            f.write(NODEFILE)

    @classmethod
    def tearDownClass(cls):
        cls._process.terminate()
        os.chdir(cls._olcwd)
        rmtree(cls._tmpwd)

    def test_fetch_series(self):
        dobjs = fetch_series(PATID, stuid='1', sernos=1, server='Server')
        assert len(dobjs) == NIMAGES
        assert all(d.PatientID == PATID for d in dobjs)

    def test_fetch_series_2(self):
        '''check multiple series don't interfere with each other'''
        dobjs = fetch_series(PATID_B, stuid='1', sernos=[1, 14], server='Server')
        assert len(dobjs) == 2*NIMAGES, '%d' % len(dobjs)
        assert all(d.PatientID == PATID_B for d in dobjs)
        assert all(d.StudyInstanceUID == STUDYUID_B for d in dobjs)
        assert set(d.SeriesInstanceUID for d in dobjs) == set([SERIESUID_B1, SERIESUID_B2])

    def test_fetch_series_to_disk(self):
        tempd = mkdtemp()
        nser = fetch_series_to_disk(
            PATID, outdir=tempd,
            studyid='1', sernos=1, server='Server', usezip=False
        )
        assert nser == 1

        dir_ = glob(join(tempd, '*'))[0]
        nfound = len(glob(join(dir_, '*')))
        assert nfound == NIMAGES

        dobjs = read_series(dir_, key=None, numeric=False, reverse=False, globspec='*')
        assert len(dobjs) == NIMAGES, '%d' % len(dobjs)

        rmtree(tempd)


if __name__ == '__main__':
    unittest.main()
