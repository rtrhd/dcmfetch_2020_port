#!/usr/bin/env python
from __future__ import print_function, division

import unittest
import sys
from os.path import join, abspath, dirname

TESTDIR = dirname(abspath(__file__))
sys.path.insert(0, abspath(join(TESTDIR, '..')))
TESTDATA = join(TESTDIR, 'testdata')

from dcmfetch.fetchdialog import dicom_date_as_slplan


class TestFetchDialog(unittest.TestCase):
    ''' Tests for functions in fetchdialog.py '''

    def test_dicom_date_as_slplan(self):
        self.assertEqual(dicom_date_as_slplan('2000-01-01'), '01-Jan-2000')
        self.assertEqual(dicom_date_as_slplan('20000101'),   '01-Jan-2000')

        self.assertEqual(dicom_date_as_slplan('2000-01-32'), '??-???-????')
        self.assertEqual(dicom_date_as_slplan('2000-13-01'), '??-???-????')

        self.assertEqual(dicom_date_as_slplan('1900-01-01'), '01-Jan-1900')
        self.assertEqual(dicom_date_as_slplan('1899-01-01'), '??-???-????')
        self.assertEqual(dicom_date_as_slplan('3000-01-01'), '01-Jan-3000')


if __name__ == '__main__':
    unittest.main()
